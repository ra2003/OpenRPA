Dear RPA-tors. Let me congratulate you with great change in the RPA world. **The first enterprise level open source RPA platform is here!**
>   The OpenRPA - free, fast and reliable

The **OpenRPA** is based on Python and using well known opensource solutions such as Selenium, OpenCV, Win32, UI automation and others. Thanks to it we were able to create consolidated platform with all possible features.

The **OpenRPA** is distributed under the MIT license which allows you to use it in any way you want and any time you need without any restrictions.

At the time of this writing the OpenRPA is successefully using in several big Russian companies. Companies in which it was decided to develop own RPA division with no dependencies on expensive licenses.

The **OpenRPA** consist of:
- WinPython 3.7.1 32-bit & 64-bit, license MIT (https://github.com/winpython/winpython)
- Selenium v..., license Apache 2.0
- pywinauto 0.6.5, license BSD 3-Clause (https://github.com/pywinauto/pywinauto) 
- Semantic UI ..., license MIT (https://github.com/Semantic-Org/Semantic-UI)
- PyAutoGUI ..., license BSD 3-Clause (https://github.com/asweigart/pyautogui)
- keyboard ..., license MIT (https://github.com/boppreh/keyboard)
- OpenCV ...
- pywin32
