# The OpenRPA Wiki
Dear friend! <br>
Welcome to the OpenRPA wiki portal. Here you can find theory & practical docs to work with first Open Source RPA platform.

## Content
In wiki you can find:
- [About OpenRPA, library dependencies and licensing](01.-About-OpenRPA,-library-dependencies-and-licensing.md)
- [Architecture (Studio, Robot, Orchestrator)](02.-Architecture-(Studio,-Robot,-Orchestrator).md)
- [How to install (system requirements)](03.-How-to-install-(system-requirements).md)
- [Tool Studio: How to use](04.1.-Tool-Studio.-How-to-use.md)
- [Tool Robot: How to use](04.2.-Tool-Robot.-How-to-use.md)
- Tool Orchestrator: How to use
- [Theory & practice: Web app access (Chrome, Firefox, Opera)](05.1.-Theory-&-practice.-Web-app-access-(Chrome,-Firefox,-Opera).md)
- [Theory & practice: Desktop app UI access (win32 and UI automation dlls)](05.2.-Theory-&-practice.-Desktop-app-UI-access-(win32-and-UI-automation-dlls).md)
- Theory & practice: Keyboard & mouse manipulation
- Theory & practice: Screen capture & image recognition

