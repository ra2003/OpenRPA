cd %~dp0
RD /S /Q "__pycache__"
RD /S /Q "build"
RD /S /Q "dist"
.\..\Resources\WPy64-3720\python-3.7.2.amd64\python.exe setup.py sdist bdist_wheel
.\..\Resources\WPy64-3720\python-3.7.2.amd64\python.exe -m twine upload dist/*
timeout 10
cd ..
Resources\WPy64-3720\python-3.7.2.amd64\python.exe -m pip install --upgrade pyOpenRPA
Resources\WPy32-3720\python-3.7.2\python.exe -m pip install --upgrade pyOpenRPA
pause >nul