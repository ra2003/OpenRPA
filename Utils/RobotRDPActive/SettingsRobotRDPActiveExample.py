#Robot RDPActive settings
from pyOpenRPA.Robot import OrchestratorConnector
from pyOpenRPA.Tools import RobotRDPActive # For RobotRDPActive folder purposes
import os
import logging
import datetime
import sys
#Definitions
lOrchestratorHost="localhost"
lOrchestratorPort=8081
lOrchestratorProtocol="http"
lOrchestratorAuthToken="1992-04-03-0643-ru-b4ff-openrpa52zzz"
gRobotName="RobotRDPActive"
lRobotRDPActiveFolderPath = '\\'.join(RobotRDPActive.__file__.split('\\')[:-1])
#print(f"{os.path.join(lRobotRDPActiveFolderPath,'SessionDefs.py')}")
def Settings():
    mDict = {
        "RDPList":
        [
            {
                "Host": "77.77.22.22",  # Host address
                "Port": "7777",  # RDP Port
                "Login": "test",  # Login
                "Password": "test",  # Password
                "Screen": {
                    "Width": 1680, #Width of the remote desktop in pixels
                    "Height": 1050, #Height of the remote desktop in pixels
                    # "640x480" or "1680x1050" or "FullScreen". If Resolution not exists set full screen
                    "FlagUseAllMonitors": False,  # True or False
                    "DepthBit": "32"  # "32" or "24" or "16" or "15"
                },
                "SharedDriveList":["c"], # List of the Root sesion hard drives
                ###### Will updated in program ############
                "SessionHex":"", # Hex is created when robot runs
                "SessionIsWindowExistBool": False , # Flag if the RDP window is exist, old name "FlagSessionIsActive". Check every n seconds
                "SessionIsWindowResponsibleBool": False # Flag if RDP window is responsible (recieve commands). Check every nn seconds. If window is Responsible - window is Exist too
            }
        ],
        "ResponsibilityCheckIntervalSec": None, # Seconds interval when Robot check the RDP responsibility. if None - dont check
        "FullScreenSessionIndex": None, #Index of the current session which is full screened, None is no session in fullscreen
        "Logger": logging.getLogger(gRobotName),
        "Logs": {
            "FlagShowInConsole":True
        },
        "OrchestratorToRobotStorage": { 
            "FullScreenSessionIndex":None, #Index of the session which is full screen in GUI. None if no session in full screen
            "IgnoreIndexList":[]
        },
        "OrchestratorToRobotResetStorage": { 
            "SafeTurnOff":False ,#Control from orchestrator to safety turn off robot
            "ActivityList":[
                #{
                #    "ModulePath": f"{os.path.join(lRobotRDPActiveFolderPath,'SessionDefs.py')}", # "Session\\SessionDefs.py"
                #    "DefName":"test", # Function name
                #    "ArgList":[1,2,3], # Args list
                #    "ArgDict":{"ttt":1,"222":2,"dsd":3} # Args dictionary
                #}
            ]
        },
        "OrchestratorConnector": {
            #Fill below
        },
        "OrchestratorConnectorTerminateAll":OrchestratorConnector.IntervalTerminateAll, #Call this function when program must be shutted down (to kill threads from OrchestratorConnector)
        "Scheduler":None # !!! Will be filled below
    }
    ###############
    # Scheduler operations for RDP
    ###############
    mDict["Scheduler"]={
        "ActivityTimeCheckLoopSeconds":5, #Количество секунд, между циклами проверки действий
        "ActivityTimeList": [
            {
                "TimeHH:MM": "22:23", #Time [HH:MM] to trigger activity
                "WeekdayList": [1,2,3,4,5,6,7], #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
                "Activity":{
                    "ModulePath": f"{os.path.join(lRobotRDPActiveFolderPath,'GlobalDictSessionIndex_Defs.py')}", # "Session\\SessionDefs.py"
                    "DefName":"ProcessStop", # Function name
                    "ArgList":[], # Args list
                    "ArgDict":{"inGlobalDict": mDict, "inSessionIndex": 0, "inProcessName": "notepad.exe", "inFlagForceClose":True} # Args dictionary
                }
            },
            {
                "TimeHH:MMStart": "12:40", #Time [HH:MM] to trigger activity
                "TimeHH:MMStop": "17:10",
                "ActivityIntervalSeconds": 5,
                "WeekdayList": [1, 2, 3, 4, 5, 6, 7], #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
                "Activity":{
                    "ModulePath": f"{os.path.join(lRobotRDPActiveFolderPath,'GlobalDictSessionIndex_Defs.py')}", # "RobotRDPActive\\SessionDefs.py"
                    "DefName":"ProcessStartIfNotRunning", # Function name
                    "ArgList":[], # Args list
                    "ArgDict":{"inGlobalDict": mDict, "inSessionIndex": 0, "inProcessName": "notepad.exe", "inFilePath": "notepad"} # Args dictionary
                }
            }
        ]
    }
    ######################
    #OrchestratorConnector
    ######################
    mDict["OrchestratorConnector"]={
        "IntervalDataSendAsync": [
            {
                "Interval": 1.1,
                "RobotStorage": mDict,
                "RobotStorageKey": "RDPList",
                "OrchestratorKeyList": ["Storage", gRobotName,"RobotToOrchestratorStorage","RDPList"],
                "OrchestratorProtocol": lOrchestratorProtocol,
                "OrchestratorHost": lOrchestratorHost,
                "OrchestratorPort": lOrchestratorPort,
                "OrchestratorAuthToken": lOrchestratorAuthToken
            },
            {
                "Interval": 1,
                "RobotStorage": mDict,
                "RobotStorageKey": "RDPList",
                "OrchestratorKeyList": ["Storage", gRobotName,"RobotToOrchestratorStorage","FullScreenSessionIndex"],
                "OrchestratorProtocol": lOrchestratorProtocol,
                "OrchestratorHost": lOrchestratorHost,
                "OrchestratorPort": lOrchestratorPort,
                "OrchestratorAuthToken": lOrchestratorAuthToken
            }
        ],
        "IntervalDataReceiveResetAsync": [
            {
                "Interval": 1.05,
                "RobotStorage": mDict,
                "RobotStorageKey": "OrchestratorToRobotResetStorage",
                "RobotResetValue": mDict["OrchestratorToRobotResetStorage"],
                "OrchestratorKeyList": ["Storage", gRobotName,"OrchestratorToRobotResetStorage"],
                "OrchestratorProtocol": lOrchestratorProtocol,
                "OrchestratorHost": lOrchestratorHost,
                "OrchestratorPort": lOrchestratorPort,
                "OrchestratorAuthToken": lOrchestratorAuthToken
            }
        ],
        "IntervalDataReceiveAsync": [
            {
                "Interval": 1.25,
                "RobotStorage": mDict,
                "RobotStorageKey": "OrchestratorToRobotStorage",
                "OrchestratorKeyList": ["Storage", gRobotName,"OrchestratorToRobotStorage"],
                "OrchestratorProtocol": lOrchestratorProtocol,
                "OrchestratorHost": lOrchestratorHost,
                "OrchestratorPort": lOrchestratorPort,
                "OrchestratorAuthToken": lOrchestratorAuthToken
            }
        ]
    }
    #Turn off many warnings from orchestrator connector logger
    OrchestratorConnector.LoggerSetLevelError()
    #Run OrchestratorConnector initialization
    OrchestratorConnector.ConfigurationInit(mDict["OrchestratorConnector"])
    #########################
    #Создать файл логирования
    # add filemode="w" to overwrite
    if not os.path.exists("Reports"):
        os.makedirs("Reports")
    ##########################
    #Подготовка логгера Robot
    #########################
    mRobotLogger=mDict["Logger"]
    mRobotLogger.setLevel(logging.INFO) #logging.DEBUG
    # create the logging file handler
    mRobotLoggerFH = logging.FileHandler("Reports\ReportRobotRDPActive_"+datetime.datetime.now().strftime("%Y_%m_%d")+".log")
    mRobotLoggerFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    mRobotLoggerFH.setFormatter(mRobotLoggerFormatter)
    # add handler to logger object
    mRobotLogger.addHandler(mRobotLoggerFH)
    ####################Add console output
    if mDict["Logs"]["FlagShowInConsole"]:
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(mRobotLoggerFormatter)
        mRobotLogger.addHandler(handler)
    ############################################
    return mDict