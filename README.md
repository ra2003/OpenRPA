# pyOpenRPA
Powerfull OpenSource RPA tool for business (based on python 3). Best perfomance and absolutely free!

## Donate
pyOpenRPA is absolutely non-commercial project. [Please donate some money if this project is important for you. Link to online donations.](https://money.yandex.ru/to/4100115560661986)

## Road map
- Wiki
  - ENG - in progress (see content below), plan date 31.08.2020
    - Translate page Theory & practice: Desktop app in english [done 19.08.2020]
    - Create page Theory & practice: Keyboard & mouse manipulation [plan 31.08.2020]
  - RUS - next step
- Tutorial
  - ENG - next step
  - RUS - in progress (see content below), plan date 18.09.2020
- Dev actions
  - Refactoring the arguments in UIDesktop.py (only in new branch pyOpenRPA version. For backward compatibility purpose), plan date -

## Wiki
In wiki you can find:
- [About pyOpenRPA, library dependencies and licensing](Wiki/01.-About-OpenRPA,-library-dependencies-and-licensing.md)
- [Architecture (Studio, Robot, Orchestrator)](Wiki/02.-Architecture-(Studio,-Robot,-Orchestrator).md)
- [How to install (system requirements)](Wiki/03.-How-to-install-(system-requirements).md)
- [Tool Studio: How to use](Wiki/04.1.-Tool-Studio.-How-to-use.md)
- [Tool Robot: How to use](Wiki/04.2.-Tool-Robot.-How-to-use.md)
- Tool Orchestrator: How to use
- [Theory & practice: Web app access (Chrome, Firefox, Opera)](Wiki/05.1.-Theory-&-practice.-Web-app-access-(Chrome,-Firefox,-Opera).md)
- [Theory & practice: Desktop app UI access (win32 and UI automation dlls)](Wiki/05.2.-Theory-&-practice.-Desktop-app-UI-access-(win32-and-UI-automation-dlls).md)
- Theory & practice: Keyboard & mouse manipulation
- Theory & practice: Screen capture & image recognition

## Tutorials
[ENG]
Content in progress<br>
[RUS]
[Перейти в раздел туториалов](Wiki/RUS_Tutorial/README.md)

# Copyrights & Contacts
pyOpenRPA is created by Ivan Maslov (Russia). Use it for free! <br>
*My purpose is to create [#IT4Business](https://www.facebook.com/RU.IT4Business) models.* <br> 
If you need IT help feel free to contact me.<br> 
## My contacts: <br>
E-mail: Ivan.Maslov@UnicodeLabs.ru<br>
Skype: MegaFinder<br>
Facebook: https://www.facebook.com/RU.Ivan.Maslov<br>
LinkedIn: https://www.linkedin.com/in/RU-IvanMaslov/

# Dependencies
*  Python 3 x32 [psutil, pywinauto, wmi, PIL, keyboard, pyautogui, win32api (pywin32), selenium, openCV, tesseract, requests, lxml, PyMuPDF]
*  Python 3 x64 [psutil, pywinauto, wmi, PIL, keyboard, pyautogui, win32api (pywin32), selenium, openCV, tesseract, requests, lxml, PyMuPDF]
*  pywinauto (Windows GUI automation)
*  Semantic UI CSS framework
*  JsRender by https://www.jsviews.com (switch to Handlebars)
*  Handlebars

#License
Under the MIT license (absolutely free)