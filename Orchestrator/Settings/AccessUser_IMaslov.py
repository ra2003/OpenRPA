# Init Section
gUserNameStr = "IMaslov" # User name
gDomainNameStr = "" # DOMAIN or EMPTY str
gDomainIsDefaultBool = True # If domain is exist and is default (default = you can type login without domain name)

def SettingsUpdate(inDict):
    lRuleDomainUserDict = {
        "MethodMatchURLBeforeList": [
            {
                "Method":"GET",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            },
            {
                "Method":"POST",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            }
        ],
        "ControlPanelKeyAllowedList": ["TestControlPanel", "RobotRDPActive","RobotScreenActive", "ControlPanel_Template"] # If empty - all is allowed
    }
    # Case add domain + user
    inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({(gDomainNameStr.upper(),gUserNameStr.upper()):lRuleDomainUserDict})
    if gDomainIsDefaultBool:
        # Case add default domain + user
        inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({("",gUserNameStr.upper()):lRuleDomainUserDict})
    #Return current dict
    return inDict