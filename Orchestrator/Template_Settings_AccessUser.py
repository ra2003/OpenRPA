# Role model - if len of keys in dict is 0 - all access. If at least len is 1 - only this access
#   "Orchestrator":{
#       "Controls": {
#           "RestartOrchestrator": {}, # Feature to restart orchestrator on virtual machine
#           "LookMachineScreenshots": {} # Feature to look machina screenshots
#       },
#       "RDPActive": { # Robot RDP active module
#           "ListRead": {} # Access to read RDP session list
#           "RestartPC": {} # Restart PC
#           "GITRestartOrchestrator": {} # Update GIT + restart orchestrator
#       }
#     }
#   }
	
# USAGE in .py
# inRequest.
# inRequest.OpenRPA["DefUserRoleAccessAsk"](["Orchestrator","RDPActive","Controls"]) - return True or False
# inRequest.OpenRPA["DefUserRoleHierarchyGet"]() - Return dict of the role hierarchy or {}

# Init Section
gUserNameStr = "Login" # User name without domain name
gDomainNameStr = "" # DOMAIN or EMPTY str if no domain
gDomainIsDefaultBool = True # If domain is exist and is default (default = you can type login without domain name)

def SettingsUpdate(inDict):
    lRuleDomainUserDict = {
        "MethodMatchURLBeforeList": [
            {
                "Method":"GET",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            },
            {
                "Method":"POST",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            }
        ],
        "ControlPanelKeyAllowedList": ["TestControlPanel", "RobotRDPActive","RobotScreenActive", "ControlPanel_Template"] # If empty - all is allowed
    }
    # Case add domain + user
    inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({(gDomainNameStr.upper(),gUserNameStr.upper()):lRuleDomainUserDict})
    if gDomainIsDefaultBool:
        # Case add default domain + user
        inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({("",gUserNameStr.upper()):lRuleDomainUserDict})
    #Return current dict
    return inDict